#import libraries
import os, sys, cv2, time, numpy, argparse, subprocess
import matplotlib.pyplot as plt

#args
video=audio=False
if len(sys.argv[1:]):
    for arg in sys.argv[1:]:
        if arg in ('-h','--help'):
            print('-h','--help','\t display help')
            print('-a','--audio','\t make sound when carriage')
            print('-v','--video','\t display camera stream')
            sys.exit(2)  
        elif arg in ('-a','--audio'):
            audio=True   
        elif arg in ('-v','--video'):
            video=True    
        else: 
            print('Use: "',sys.argv[0],'--help " to display more informations.')
            sys.exit(2)

#path to directory with this script
mainPath = os.path.dirname(os.path.realpath(__file__))

#path to directory with output images/videos
outputDir=mainPath+'/out'
        
#path to video
videoPath='http://hls7.webcamera.pl/krakow_cam_702b61/krakow_cam_702b61.stream/playlist.m3u8'

#load cascade
cascade = cv2.CascadeClassifier('cascadeb5.xml')


# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(outputDir+'/tmp.avi',fourcc, 25, (1280, 720))


#capture stream
#cap = cv2.VideoCapture(videoPath)
cap = cv2.VideoCapture('sample1.avi')
#cap = cv2.VideoCapture('output2.avi')

counter=0
carriagePassing=False

def tic():
    global startTim
    startTim=time.time()
    
def toc():
    return (time.time() -startTim)
    
def mean(wheels):
    sumy=sumx=0
    for (x,y,w,h) in wheels:
        y+=cutY;sumy+=y
        x+=cutX;sumx+=x
        #cv2.rectangle(frame,(x,y),(x+5,y+5),(255,233,33),2)
    posx=int(sumx/len(wheels))
    posy=int(sumy/len(wheels)) 
    return posx, posy
def reconnect():
    global cap
    print('Network-manager restart at', time.strftime("%m-%d %H:%M:%S"))
    subprocess.Popen("cvlc --play-and-exit sound.mp3", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    subprocess.Popen("service network-manager restart", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cap.release()
    cv2.destroyAllWindows()
    cap = cv2.VideoCapture(videoPath)
    time.sleep(15)
    
toSave=10;#how many frames consecutively with car to save
toReset=15;#how many frames consecutively without regisered car to reset wheelON
wheelON=0#how many frames consecutively with wheel on screen
wheelOFF=0#how many frames consecutively without wheel

#cut frame
cutX=600
cutY=500

#last carriage vid
pastFrames=[]

#positions for last carriage
xAx=[]
yAx=[]

down=left=0
posx=posy=0
confirm=False


while True:#cap.isOpened():
    try:        
        #load next frame
        status, frame = cap.read()
        
        #if not connected
        if not status:
            reconnect()
            continue
            
        #save output with some info to file
        cv2.putText(frame,'{0}'.format(time.strftime("%m-%d %H:%M:%S")),(50,600), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        cv2.putText(frame,str(counter),(50,670), cv2.FONT_HERSHEY_SIMPLEX, 2, (11,255,255), 7, cv2.LINE_AA)
        out.write(frame)    
        
        #convert to grayscale and cut
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)[cutY:, cutX:]
        
        #detect wheels    
        wheels = cascade.detectMultiScale(gray, scaleFactor=2, minNeighbors=10, minSize=(20, 20),maxSize=(100, 100))
                
        if not carriagePassing:
            if len(wheels)>2:
                wheelON+=1
                wheelOFF=0 
                if wheelON==toSave:#enough to save
                    frameToSave=frame#.copy()
                    pastFrames=[]
                    tic()#start timer
                    carriagePassing=True
                    #save position
                    xAx.append(posx)
                    yAx.append(posy)
            if not len(wheels):
                wheelOFF+=1
                if not wheelOFF%toReset: wheelON=0

        elif carriagePassing:
                #pastFrames.append(frame)#.copy()) 
                if len(wheels):
                     posx, posy = mean(wheels)
                     cv2.rectangle(frame,(posx,posy),(posx+10,posy+10),(255,0,0),2)
                     xAx.append(posx)
                     yAx.append(posy)
                     wheelOFF=0
                     wheelON+=1
                     if not confirm and len(wheels)>2:
                        if audio: subprocess.Popen("cvlc --play-and-exit sound.mp3", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        confirm=True
                elif not len(wheels):
                    wheelOFF+=1
                    if not wheelOFF%toReset:
                        wheelON=0
                        carriagePassing=False
                        if toc()>=3 and confirm:
                            #===================================
                            tmp=0
                            for i in xAx[:int(len(xAx)/2)]:tmp+=i     
                            sr1=tmp/(len(xAx)/2) 
                            
                            tmp=0
                            for i in xAx[int(len(xAx)/2):]: tmp+=i
                            sr2=tmp/(len(xAx)/2) 
                            left=int(sr1-sr2)
                            #===================================
                            tmp=0
                            for i in yAx[:int(len(xAx)/2)]:tmp+=i     
                            sr1=tmp/(len(xAx)/2) 
                            
                            tmp=0
                            for i in yAx[int(len(xAx)/2):]:tmp+=i     
                            sr2=tmp/(len(xAx)/2) 
                            down=int(sr2-sr1)
                            #===================================
                           
                            if left>-60 and down>-15: 
                                print('saving:',time.strftime("%m-%d %H:%M:%S"))
                                counter+=1#for single
                                frameName=time.strftime("%m-%d %H:%M:%S")+'.jpg'
                                cv2.imwrite(outputDir+'/pos/'+frameName,frameToSave)
                                #for frame in pastFrames: out.write(frame)    
                        pastFrames=[]
                        confirm=False

                        xAx=[]
                        yAx=[]       
                         
        cv2.putText(frame,'len(wheels):     '+str(len(wheels)),(50,50),  cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        cv2.putText(frame,'wheelOFF:       '+str(wheelOFF),  (50,100), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        cv2.putText(frame,'wheelON:        '+str(wheelON),   (50,150), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        cv2.putText(frame,'carriagePassing: '+str(carriagePassing),(50,200), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        cv2.putText(frame,str(counter),(500,200), cv2.FONT_HERSHEY_SIMPLEX, 8, (11,255,255), 7, cv2.LINE_AA)
        if carriagePassing: cv2.putText(frame,'Time: {0:.2f}'.format(toc()),(50,450), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)
        #cv2.putText(frame,'last({}, {}) {} {} {}'.format(left,down,once3, once4,confirm),(50,250), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)                
        cv2.putText(frame,'{0}'.format(time.strftime("%m-%d %H:%M:%S")),(50,350), cv2.FONT_HERSHEY_SIMPLEX, 1, (11,255,255),4)  
        #display frame 
        if video: cv2.imshow('frame',frame)

        #frameName=time.strftime("%m-%d %H:%M:%S")+'.jpg' 
        #cv2.imwrite(outputDir+'/pos/'+frameName,frame)
        if (cv2.waitKey(2) & 0xff) == ord('\x1b'): break
    except Exception as err:
        #raise
        print(str(err))
            

cap.release()
out.release()
cv2.destroyAllWindows()

'''
circles = cv2.HoughCircles(frame, cv2.HOUGH_GRADIENT, 1.2, 100)
if circles is not None:
    circles = numpy.round(circles[0, :]).astype("int")
    for (x, y, r) in circles:
        cv2.circle(frame, (x, y), r, (0, 255, 0), 4)
'''
