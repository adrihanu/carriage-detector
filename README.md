# Carriage detector

Program and thesis about Haar feature-based cascade classifier for detecting carriages from video stream.

Tags: `Python`, `OpenCV`, `Haar cascade`
